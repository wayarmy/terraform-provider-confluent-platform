# Getting started

## 1. Download dependency

```shell script
go mod vendor
```

## 2. Build

```
go build -o terraform-provider-confluent-kafka_v0.1.0
mv confluent_kafka $HOME/.terraform.d/plugins/registry.terraform.io/hashicorp/confluent-kafka/0.1.0/darwin_amd64
```

## 3. Start your first terraform plan

Create `main.tf` file:

```shell script
terraform {
  required_providers {
    confluent-kafka = {
      version = "0.1.0"
      source = "confluent-kafka"
    }
  }
}
```

Init

```shell script
terraform init
```

Plan

```shell script
terraform plan
```

Apply

```shell script
terraform apply
```

## 4. Resources supported

- Define the provider

```shell
provider "confluent-kafka" {
  alias = "confluent" # Alias of the provider
  bootstrap_servers = ["localhost:9093"] # List of Kafka bootstrap servers
  ca_cert           = "certs/ca.pem" # CA cert to connect to Kafka cluster
  client_cert       = "certs/cert.pem" # Client certs to connect to Kafka cluster
  client_key        = "certs/key.pem" # Client private key to connect to Kafka cluster
  skip_tls_verify   = true # Skip TlS verification
  username = "xxxx" # LDAP or SASL username to connect to Confluent or MDS
  password = "yyyy" # LDAP or SASL password to connect to Confluent or MDS
}
```

### 4.1 Topics

- Topic Example

```shell script
resource "kafka_topic" "example_topic" {
  cluster_id = "5-2KZs0YRYCVv8YCDdfSVw" # Optional: If not, terraform will use first cluster in the cluster list
  name = "test5-terraform-confluent-provider" # Topic name
  replication_factor = 3 # Replication factor
  partitions = 5 # The number of partition

  config = {
    "segment.ms" = "20000"
    "cleanup.policy" = "compact"
  }
  provider = confluent-kafka.confluent
}
```

### 4.2 Cluster role binding

- Will describe and bind the cluster role to principal (User or scope)

- Example:

```shell
resource "cluster_role_binding" "example_role_binding" {
  cluster_id = "5-2KZs0YRYCVv8YCDdfSVw" # Optional: If not, terraform will use first cluster in the cluster list
  role = "UserAdmin" # Allow roles: "AuditAdmin", "ClusterAdmin", "DeveloperManage", "DeveloperRead", "DeveloperWrite", "Operator", "ResourceOwner", "SecurityAdmin", "SystemAdmin", "UserAdmin",
  principal = "User:manh.do" # Allow convention: User:<user_name> or Group:<group_name>
  cluster_type = "Kafka" # Support 4 types of clusters: Kafka, SchemaRegistry, KSQL, Connect
  provider = confluent-kafka.confluent
}
```

### 4.3 Kafka topic RBAC

- Will describe and bind the resource role (Not Cluster role) to principal

- Example:

```shell
resource "kafka_topic_rbac" "example_topic_rbac" {
  principal = "User:manh.do" # Allow convention: User:<user_name>, Group:<group_name>, User:CN=<domain>
  role = "ResourceOwner" # Allow roles: "DeveloperRead", "DeveloperWrite", "Operator", "ResourceOwner"
  resource_type = "Topic" # Allow only: Topic
  pattern_type = "PREFIXED" # Allow: PREFIXED and LITERAL
  name = "test5-" # The pattern contains in topic name
  cluster_id = "5-2KZs0YRYCVv8YCDdfSVw" # Optional: If not, terraform will use first cluster in the cluster list
  provider = confluent-kafka.confluent
}

```

## 5. Contributing

- Clone this project

```shell
git clone git@gitlab.com:wayarmy/terraform-provider-confluent-kafka.git
```

- Build your personal environment terraform plugin for testing
```shell
./build.sh
```

- Run your test before push your code

```shell
go test
```

## 6. Build release

```shell
make all
```
