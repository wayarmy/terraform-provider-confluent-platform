#!/bin/bash

set -e

rm -fr test/.terraform
rm -fr test/.terraform.lock.hcl

FILE_NAME="terraform-provider-confluent-kafka_v0.1.0"
DIR=$HOME/.terraform.d/plugins/registry.terraform.io/hashicorp/confluent-kafka/0.1.0/darwin_amd64
mkdir -p $DIR

go build -o $FILE_NAME main.go

# zip ${FILE_NAME}_darwin_amd64.zip $FILE_NAME

mv ${FILE_NAME} $DIR

(cd test && terraform init -get=false)