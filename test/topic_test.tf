resource "cluster_role_binding" "example_role_binding" {
  cluster_id = "xxxx"
  role = "UserAdmin"
  principal = "User:wayarmy"
  cluster_type = "Kafka"
  provider = confluent-kafka.confluent
}

resource "cluster_role_binding" "example_role_binding_operator" {
  cluster_id = "xxxxx"
  role = "ClusterAdmin"
  principal = "User:wayarmy"
  cluster_type = "Kafka"
  provider = confluent-kafka.confluent
}

resource "kafka_topic_rbac" "example_topic_rbac" {
  principal = "User:wayarmy"
  role = "ResourceOwner"
  resource_type = "Topic"
  pattern_type = "PREFIXED"
  name = "test-"
  cluster_id = "xxxx"
  provider = confluent-kafka.confluent
}

resource "kafka_topic" "example_topic" {
  name = "test-terraform-confluent-provider"
  replication_factor = 3
  partitions = 5

  config = {
    "segment.ms" = "20000"
    "cleanup.policy" = "compact"
  }
  provider = confluent-kafka.confluent
}