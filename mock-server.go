package main

import (
	"log"
	"net/http"
	"net/http/httputil"
	"os"
)

func main_mock() {
	log.SetOutput(os.Stdout)
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		b, err := httputil.DumpRequest(request, true)
		if err != nil {
			panic(err)
		}
		log.Printf(">>> request: \n%s", string(b))
		if request.RequestURI == "/security/1.0/authenticate" {
			writer.Write([]byte(`
				{
					"auth_token": "abcdefghizk",
					"auth_type": "Basic Auth",
					"expires_in": 3600
				}
			`))
			return
		}
		if request.RequestURI == "/kafka/v3/clusters" {
			writer.Write([]byte(`
		{
			"kind": "KafkaClusterList",
			"metadata": {
				"self": "http://localhost:9391/v3/clusters",
				"next": null
			},
			"data": [
				{
					"kind": "KafkaCluster",
					"metadata": {
						"self": "http://localhost:9391/v3/clusters/cluster-1",
						"resource_name": "crn:///kafka=cluster-1"
					},
					"cluster_id": "cluster-1",
					"controller": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/brokers/1"
					},
					"acls": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/acls"
					},
					"brokers": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/brokers"
					},
					"broker_configs": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/broker-configs"
					},
					"consumer_groups": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/consumer-groups"
					},
					"topics": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/topics"
					},
					"partition_reassignments": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/topics/-/partitions/-/reassignment"
					}
				}
			]
		}
`))
			return
		}
		if request.RequestURI == "/kafka/v3/clusters/topics" {
			writer.Write([]byte(`
		{
			"kind": "KafkaClusterList",
			"metadata": {
				"self": "http://localhost:9391/v3/clusters",
				"next": null
			},
			"data": [
				{
					"kind": "KafkaCluster",
					"metadata": {
						"self": "http://localhost:9391/v3/clusters/cluster-1",
						"resource_name": "crn:///kafka=cluster-1"
					},
					"cluster_id": "cluster-1",
					"controller": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/brokers/1"
					},
					"acls": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/acls"
					},
					"brokers": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/brokers"
					},
					"broker_configs": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/broker-configs"
					},
					"consumer_groups": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/consumer-groups"
					},
					"topics": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/topics"
					},
					"partition_reassignments": {
						"related": "http://localhost:9391/v3/clusters/cluster-1/topics/-/partitions/-/reassignment"
					}
				}
			]
		}
`))
			return
		}
		if request.RequestURI == "/kafka/v3/clusters/cluster-1/topics" {
			writer.Write([]byte(`
			{
				"kind": "KafkaTopic",
				"metadata": {
					"self": "http://localhost:9391/v3/clusters/cluster-1/topics/topic-X",
					"resource_name": "crn:///kafka=cluster-1/topic=topic-X"
				},
				"cluster_id": "cluster-1",
				"topic_name": "topic-X",
				"is_internal": false,
				"replication_factor": 3,
				"partitions": {
					"related": "http://localhost:9391/v3/clusters/cluster-1/topics/topic-X/partitions"
				},
				"configs": {
					"related": "http://localhost:9391/v3/clusters/cluster-1/topics/topic-X/configs"
				},
				"partition_reassignments": {
					"related": "http://localhost:9391/v3/clusters/cluster-1/topics/topic-X/partitions/-/reassignments"
				}
			}
`))
			return
		}
		writer.Write([]byte("{}"))
	})
	err := http.ListenAndServe("0.0.0.0:8090", nil)
	if err != nil {
		panic(err)
	}
}
