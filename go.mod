module gitlab.com/wayarmy/terraform-provider-confluent-kafka

go 1.16

require (
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.7.0
	gitlab.com/wayarmy/confluent-go-client v0.0.0-20210717151553-72929cb7db85
)
