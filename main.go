package main

import (
	"github.com/hashicorp/terraform-plugin-sdk/v2/plugin"
	c "gitlab.com/wayarmy/terraform-provider-confluent-kafka/cplatform"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{ProviderFunc: c.Provider})
}
